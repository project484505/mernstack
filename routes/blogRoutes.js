const express = require('express');
const {
    getBlogs,
    createBlog,
    getBlogById,
    updateBlog,
    deleteBlog,
} = require('../controllers/blogController');
const { protect } = require('../middleware/authMiddleware');
const router = express.Router();

router.route('/').get(getBlogs).post(protect, createBlog);
router
    .route('/:id')
    .get(getBlogById)
    .put(protect, updateBlog)
    .delete(protect, deleteBlog);

module.exports = router;
